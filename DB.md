# Socialwelfare | SW

## 1-nji işler

Birinji ýa-da gapdaldan database design işleri başlabermeli.

Onyň üçin SW-daky işgäler we ekspertlar bilen gürleşip APP üçin haýsy TABLE we Entity gerekdigini düşünjek bolmaly.

We gapdaldan SW-daky köne bazany analizdan geçirmeli. Şuwagtdaky baza MSSQL-de işläp dur.
Öňki bazany loсalda galdyrmak uçin gerek zatlar:

1. 250-300 GB boş ýer gerek.
2. [SQL SERVER](https://www.guru99.com/download-install-sql-server.html)
3. [SQL SERVER MANAGEMENT STUDIO](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15).

Analiz geçirip içindäki СПРАВОЧНИК (Lising üçin ulanylýan datalar) table-lary ýygnamaly we laravel içinde migration-lary ýazyp başlasak gowy bolar.

Gerekli listing data bar bolsa onam SEED-a geçirip bolsa geçirmeli. 

## DB

SW bazasy uly, hazirki wagtda her etrap uçin 73 GB data bar we 43 sany etrap bar.

Şol sebapli biz bazanyň nagruzkasyny alyp çykar ýaly iki sany çözgi bar.

### 1. SQL Server replication

MASTER-SLAVE REPLICATION.

Iki sany server döredilýar. Esasy MASTER we SLAVE ikinjisi.

MASTER - CREATE, UPDATE, INSERT üçin.
Bu serwere hemme esasy operasiýalar uçin. Meselem SW-daky INSPEKTORLAR üçin.

SLAVE - READ üçin. Diňe uly QUERY-lar üçin. Meselem Buhgalteriýanyň otçyotlar uçin.

Ýokardaky shema hem aňsat we BACKUP hökmünde ulanyp bolar.

### 2. Data Sharding

Bu shemada uly table-lary iki-üç serwerlara paýlap bolyar.

Meselem SW Database-da men pikirimçe iň uly table TÖLEGLER table bolup biler.

Şol table etrap boyunça paylasak dogry bolar. 

Onyň uçin SW-daky iň esasy ID generate edende başdaky 2 ya 3 sifr boyunça paylanyş logikasany  düzüp bolar.